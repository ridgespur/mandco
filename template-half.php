<?php
/*
Template Name: Half Full
*/

query_posts( array ( 'category_name' => 'half-full' ) ); //'category_name' => 'acquisitions' ?> 

<?php while (have_posts()) : the_post(); ?>
    
    <div class="col-lg-6 pad-top-2">
	    <article <?php post_class(); ?>>    
		  <header>
		    <h2 class="entry-title bold"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		    <p><?php echo get_the_date(); ?></p>
		  </header>

		  <?php 
		  	if ( has_post_thumbnail() ) {
				echo the_post_thumbnail( array(225,225), array( 'class' => 'img-responsive' ) );
			} ?>

		  <div class="entry-summary">
		  	<?php $one_liner = get_field('one_liner');?>
		  	<em><?php echo $one_liner; ?></em>
		    <p class="light"><?php the_excerpt(); ?></p>
		    
		  </div>		
		</article>

		<?php 
			$counter++;
        	if ($counter % 2 == 0) {
         	echo '</div><div class="clearfix">';
        	}
        ?>

	</div>

<?php endwhile; ?>

<?php wp_reset_query(); ?>