<?php
/*
Template Name: Test
*/

?>
<?php query_posts( array ( 'category_name' => 'showcases' ) ); //'category_name' => 'acquisitions' ?> 

<?php while (have_posts()) : the_post(); ?>
    
    <div class="col-lg-4 pad-top-2">
	    <article <?php post_class(); ?>>    
		  <header>
		    <h2 class="entry-title bold"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		  </header>

		  <?php 
		  	if ( has_post_thumbnail() ) {
				echo the_post_thumbnail( array(225,225), array( 'class' => 'img-responsive col-lg-12 pull-left' ) );
			} ?>

		  <div class="entry-summary">
		    <p class="light"><?php the_excerpt(); ?></p>
		  </div>		
		</article>
	</div>

<?php endwhile; ?>

<?php wp_reset_query(); ?>