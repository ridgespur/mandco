<?php
/*
Template Name: Organization Footer
*/

?>

<?php
if ( have_posts() ) {
        while ( have_posts() ) {
                the_post();
                the_content();
        } // end while
} // end if
?>


<?php get_template_part('templates/org'); ?>