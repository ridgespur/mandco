<?php
/*
Template Name: Our Firm
*/
?>


<div class="container pad-top-2 pad-bot firm">
	
	<div class="col-lg-4 text-center">
		<img class="img-responsive img-center avatar" src="<?php echo get_template_directory_uri(); ?>/assets/img/rip.png">
		<div>
			<div>
			  <h4 class="panel-title pad-top pad-bot">
			    <a href="<?php echo site_url(); ?>/our-firms/ronald-muhlenhaupt/">
			      RONALD P. “RIP” MUHLENHAUPT
			    </a>
			  </h4>
			</div>
			<div>
			  <div class="text-justify pad-bot team-bio">
			    Over the course of his 35-year career, Rip has spearheaded successful management and marketing solutions for his family businesses, subsequent start-ups and public companies in a variety of consumer and B2B sectors.
			  </div>
			</div>
		</div>
		<hr class="featurette-divider red">
		<p class="thin large">Principal</p>
		<a href="mailto:rip@muhlenhaupt.com" class="gray" target="_blank"><i class="fa fa-envelope" style="padding-right:5px;"></i></a>
		<a href="https://www.linkedin.com/in/ripmuhlenhaupt" target="_blank"><i class="fa fa-linkedin"></i></a>
	</div>

	<div class="col-lg-4 text-center">
		<img class="img-responsive img-center avatar" src="<?php echo get_template_directory_uri(); ?>/assets/img/adam.png">
		<!-- <h4>TARA M. SIRI</h4> -->
		<!-- <div class="panel-group pad-top" id="accordion" role="tablist" aria-multiselectable="true"> -->
		<div>
			<div>
			  <h4 class="panel-title pad-top pad-bot">
			    <a href="<?php echo site_url(); ?>/our-firms/adam-p-muhlenhaupt/">
			      ADAM P. MUHLENHAUPT
			    </a>
			  </h4>
			</div>
			<div id="collapseTwo" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingTwo">
			  <div class="text-justify pad-bot team-bio">
			    While he brings both keen wit and youthful insight to organizational development issues, Adam's innate creativity and hard work is evident in all work being produced by Muhlenhaupt + Company today, as well as the ever-growing team.
			  </div>
			</div>
		</div>
		<!-- </div> -->
		<hr class="featurette-divider red">
		<p class="thin large">Principal</p>
		<a href="mailto:adam@muhlenhaupt.com" class="gray" target="_blank"><i class="fa fa-envelope" style="padding-right:5px;"></i></a>
		<a href="http://www.linkedin.com/pub/adam-p-muhlenhaupt/45/343/2bb" target="_blank"><i class="fa fa-linkedin"></i></a>
	</div>

	<div class="col-lg-4 text-center">
		<img class="img-responsive img-center avatar" src="<?php echo get_template_directory_uri(); ?>/assets/img/tara.png">
		<div>
			<div>
			  <h4 class="panel-title pad-top pad-bot">
			    <a href="<?php echo site_url(); ?>/our-firms/tara-m-siri/">
			      TARA M. SIRI
			    </a>
			  </h4>
			</div>
			<div>
			  <div class="text-justify pad-bot team-bio">
			    Tara's ultimate responsibility lies in the accurate deployment of all client deliverables, leveraging today's latest marketing platforms, allowing an ever-expanding presence in corporate environments.
			  </div>
			</div>
		</div>
		<hr class="featurette-divider red">
		<p class="thin large">Director, Creative Comm.</p>
		<a href="mailto:tara@muhlenhaupt.com" class="gray" target="_blank"><i class="fa fa-envelope" style="padding-right:5px;"></i></a>
		<a href="http://www.linkedin.com/pub/tara-m-siri/49/747/537" target="_blank"><i class="fa fa-linkedin"></i></a>
	</div>

	<div class="clearfix"></div>
	
	<div class="col-lg-4 text-center">
		<p class="pad-top">&nbsp;</p>
		<img class="img-responsive img-center avatar" src="<?php echo get_template_directory_uri(); ?>/assets/img/naz.png">
		<!-- <div class="panel-group pad-top" id="accordion" role="tablist" aria-multiselectable="true"> -->
		<div>
			<div>
			  <h4 class="panel-title pad-top pad-bot">
			    <a>
			      NAZARENA LUZZI CASTRO
			    </a>
			  </h4>
			</div>
			<div>
			  <div class="text-justify pad-bot team-bio">
			    The most creative young lady you'll ever meet. Naz is a critical piece to the success of our firm. Her creativity is at the heart of many of our initiatives and clearly evident in our final products.
			  </div>
			</div>
		</div>
		<!-- </div> -->
		<hr class="featurette-divider red">
		<p class="thin large">Creative Partner</p>
		<a href="mailto:naz@muhlenhaupt.com" class="gray" target="_blank"><i class="fa fa-envelope" style="padding-right:5px;"></i></a>
		<a href="http://www.linkedin.com/pub/nazarena-luzzi-castro/54/645/566" target="_blank"><i class="fa fa-linkedin"></i></a>
	</div>

	<!-- <div class="col-lg-4 text-center">
		<p class="pad-top">&nbsp;</p>
		<img class="img-responsive img-center avatar img-circle" src="<?php echo get_template_directory_uri(); ?>/assets/img/josh.jpg">
		<div>
			<div>
			  <h4 class="panel-title pad-top pad-bot">
			    <a>
			      JOSH METH
			    </a>
			  </h4>
			</div>
			<div>
			  <div class="text-justify pad-bot team-bio">
			    Dedicated to our success on the digital front. An expert web developer and digital marketer. Josh relocated to Philadelphia to be in a creative city, rich with an entrepreneutiral community. Forever fascinated with new technology and companies that change the world as we know it.
			  </div>
			</div>
		</div>
		<hr class="featurette-divider red">
		<p class="thin large">Digital Performance Partner</p>
		<a href="mailto:josh@muhlenhaupt.com" class="gray" target="_blank"><i class="fa fa-envelope" style="padding-right:5px;"></i></a>
		<a href="http://www.linkedin.com/pub/josh-meth/39/729/37" target="_blank"><i class="fa fa-linkedin"></i></a>
	</div> -->

	<div class="col-lg-4 text-center">
		<p class="pad-top">&nbsp;</p>
		<img class="img-responsive img-center avatar" src="<?php echo get_template_directory_uri(); ?>/assets/img/SarahLarson_officephoto.png">
		  <div>
		    <div>
		      <h4 class="panel-title pad-top pad-bot">
		        <a>
		          SARAH LARSON
		        </a>
		      </h4>
		    </div>
		    <div>
		      <div class="text-justify pad-bot team-bio">
		        Sarah Larson is a recent Graduate of La Salle University with degrees in Marketing and Digital Arts. Sarah's role in managing and distributing content across our clients' websites ​makes her a critical part of effective search engine optimization strategy and execution. Specializing in digital media and channeling positivity - Sarah's role will be important in all things "social" here at M+Co.
		      </div>
		    </div>
		  </div>
		<hr class="featurette-divider red">
		<p class="thin large">Digital Content Manager</p>
		<a href="mailto:sarah.larson@muhlenhaupt.com" class="gray" target="_blank"><i class="fa fa-envelope" style="padding-right:5px;"></i></a>
		<a href="https://www.linkedin.com/in/larsonsarah" target="_blank"><i class="fa fa-linkedin"></i></a>
	</div>

	<div class="col-lg-4 text-center">
		<p class="pad-top">&nbsp;</p>
		<img class="img-responsive img-center avatar" src="<?php echo get_template_directory_uri(); ?>/assets/img/MeghanPhoto.png">
		  <div>
		    <div>
		      <h4 class="panel-title pad-top pad-bot">
		        <a>
		          MEGHAN LONGO
		        </a>
		      </h4>
		    </div>
		    <div>
		      <div class="text-justify pad-bot team-bio">
		        Meghan is responsible for all financial transactions of the firm. Ensuring adequate records, controls and budgets. Meghan assists the principals of the firm to mitigate risk and enhance the accuracy of financial results.
		      </div>
		    </div>
		  </div>
		<hr class="featurette-divider red">
		<p class="thin large">Controller</p>
		<a href="mailto:meghan@muhlenhaupt.com" class="gray" target="_blank"><i class="fa fa-envelope" style="padding-right:5px;"></i></a>
		<!-- <a href="https://www.linkedin.com/in/larsonsarah" target="_blank"><i class="fa fa-linkedin"></i></a> -->
	</div>

	<div class="col-lg-4 text-center">
		<p class="pad-top">&nbsp;</p>
		<img class="img-responsive img-center avatar" src="<?php echo get_template_directory_uri(); ?>/assets/img/ryan-draving.png">
		  <div>
		    <div>
		      <h4 class="panel-title pad-top pad-bot">
		        <a>
		          RYAN DRAVING
		        </a>
		      </h4>
		    </div>
		    <div>
		      <div class="text-justify pad-bot team-bio">
				As Partner at Muhlenhaupt & Co, Ryan uses his industry-recognized talents and deep experience to accelerate client growth and develop effective strategies. He brings extensive combined experience in outbound and inbound lead generation to help Muhlenhaupt & Co clients build and maintain growth through inbound marketing and outbound prospecting.
		      </div>
		    </div>
		  </div>
		<hr class="featurette-divider red">
		<p class="thin large">Client Strategy Partner</p>
		<a href="mailto:ryan.draving@muhlenhaupt.com" class="gray" target="_blank"><i class="fa fa-envelope" style="padding-right:5px;"></i></a>
		<a href="https://www.linkedin.com/in/ryandraving" target="_blank"><i class="fa fa-linkedin"></i></a>
	</div>
	
</div>

<?php get_template_part('templates/org'); ?>