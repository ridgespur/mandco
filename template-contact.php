<?php
/*
Template Name: Contact
*/
?>

<div class="pad-top-2 pad-bot">
	
	
	<div class="col-lg-6">
		
		<div class="embed-responsive embed-responsive-4by3" style="height: 521px;">
			<iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3052.975710348423!2d-75.28769849999999!3d40.075953899999995!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c6be8cd5e91e53%3A0xb4f9f905282f2410!2s1100+E+Hector+St+%23200%2C+Conshohocken%2C+PA+19428!5e0!3m2!1sen!2sus!4v1431353029067" width="600" height="450" frameborder="0" style="border:0"></iframe>
		</div>
		<!-- <h4 class="text-center">Muhlenhaupt + Company, LLC  <br>  1100 East Hector Street, Suite 200, Conshohocken, PA 19428</h4 class="text-center"> -->
	</div>

	<div class="col-lg-6">
		<div class="black-bg row row-centered">
			<div class="col-lg-10 col-centered text-left pad-top pad-bot">
				<h3><span class="white">LET US HELP YOUR</span> <span class="red">ORGANIZATION MOVE FORWARD.</span></h3>
				<?php echo do_shortcode('[contact-form-7 id="57" title="Contact form 1"]'); ?>
			</div>
		</div>
	</div>

	
</div>