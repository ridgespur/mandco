<?php
/*
Template Name: Showcases
*/
$showcase_liner = get_field('showcase_liner');

query_posts( array ( 'category_name' => 'showcases' ) ); //'category_name' => 'acquisitions' ?> 

<?php while (have_posts()) : the_post(); ?>
    
    <div class="col-lg-6 pad-top-2">
	    <article <?php post_class(); ?>>    
		  <header>
		    <h2 class="entry-title bold"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
		  </header>

		  <?php 
		  	if ( has_post_thumbnail() ) {
				echo the_post_thumbnail( array(225,225), array( 'class' => 'img-responsive pull-left showcase-padding' ) );
			} ?>

		  <div class="entry-summary">
		  	<?php $one_liner = get_field('one_liner');
			
			if ($one_liner == null) {
				echo '<p class="normal">' . get_the_excerpt() . '...</p>'; 
			} else 
				echo '<p class="normal">' . $one_liner . '</p>'; 
			?>
		    <!-- <p class="light"><?php //the_excerpt(); ?></p> -->
		    <h4><a href="<?php echo get_permalink(); ?>">Continued</a></h4>
		  </div>		
		</article>
		<?php 
			$counter++;
        	if ($counter % 2 == 0) {
         	echo '</div><div class="clearfix">';
        	}
        ?>
	</div>

<?php endwhile; ?>

<?php wp_reset_query(); ?>