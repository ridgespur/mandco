<?php
/*
Template Name: Homepage
*/
?>

<div class="pad-top-2 pad-bot-2">
	
	<a href="<?php get_site_url(); ?>offerings/branding-communications/">
	<div>
		<div class="col-lg-3">
			<img class="offerings img-responsive img-center pad-bot" src="<?php echo get_template_directory_uri(); ?>/assets/img/branding.png">
		</div>

		<div class="col-lg-9">
			<hr class="featurette-divider red hp-line">
			<h2 class="red uppercase lato bold">Branding + Communications</h2>
			<hr class="featurette-divider red hp-line bottom">
			<p class="black">
			identity creation + graphics design<br>
			direction mapping<br>
			communications strategies<br>
			scripting + copywriting
			</p>
		</div>
	</div>
	</a>

	<div class="clearfix"></div>
	
	<a href="<?php get_site_url(); ?>offerings/sales-marketing/">
	<div class="pad-top-2">
		<div class="col-lg-3">
			<img class="offerings img-responsive img-center" src="<?php echo get_template_directory_uri(); ?>/assets/img/sales.png">
		</div>

		<div class="col-lg-9">
			<hr class="featurette-divider red hp-line">
			<h2 class="red uppercase lato bold">Sales + Marketing</h2>
			<hr class="featurette-divider red hp-line">
			<p class="black">
			web design, development, deployment<br>
			search engine optimization + marketing<br>
			social media strategy, design + management<br>
			promotions design + implementation<br>
			brand development
			</p>
		</div>
	</div>
	</a>

	<div class="clearfix"></div>
	
	<a href="<?php get_site_url(); ?>showcases/">
	<div class="pad-top-2">
		<div class="col-lg-3">
			<img class="offerings img-responsive img-center" src="<?php echo get_template_directory_uri(); ?>/assets/img/events.png">
		</div>

		<div class="col-lg-9">
			<hr class="featurette-divider red hp-line">
			<h2 class="red uppercase lato bold">Events Orchestration</h2>
			<hr class="featurette-divider red hp-line">
			<p class="black">
			talent selection + arrangement<br>
			venue selection + contracting<br>
			strategy development + implementation<br>
			success celebration<br>
			tradeshows Design and Coordination
			</p>
		</div>
	</div>
	</a>

	<div class="clearfix"></div>
	
	<a href="<?php get_site_url(); ?>offerings/training-education/">
	<div class="pad-top-2">
		<div class="col-lg-3">
			<img class="offerings img-responsive img-center pad-bot" src="<?php echo get_template_directory_uri(); ?>/assets/img/training.png">
		</div>

		<div class="col-lg-9">
			<hr class="featurette-divider red hp-line">
			<h2 class="red uppercase lato bold">Training + Education</h2>
			<hr class="featurette-divider red hp-line">
			<p class="black">
			customer response<br>
			call center development<br>
			motivation + leadership<br>
			quality assurance<br>
			team building
			</p>
		</div>
	</div>
	</a>


</div>


