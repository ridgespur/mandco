<?php
/*
Template Name: Offerings
*/

?>

<div class="pad-top pad-bot offerings-img container">

	<div class="col-lg-6 pad-bot pad-top">
		<div class="col-lg-4"><img class="offerings img-responsive img-center pad-bot" src="<?php echo get_template_directory_uri(); ?>/assets/img/branding.png"></div>
		<div class="col-lg-8">
		<hr class="featurette-divider red">
		<h3 class="text-center uppercase">Branding + Communications</h3>
		<hr class="featurette-divider red">
		
		<p class="text-center normal large"><em>
			identity creation + graphics design<br>
			direction mapping<br>
			communications strategies<br>
			scripting + copywriting</em>
		</p>
		</div>
	</div>

	<div class="col-lg-6 pad-bot pad-top">
		<div class="col-lg-4"><img class="offerings img-responsive img-center" src="<?php echo get_template_directory_uri(); ?>/assets/img/sales.png"></div>
		<div class="col-lg-8">
		<hr class="featurette-divider red">
		<h3 class="text-center uppercase">Sales + Marketing</h3>
		<hr class="featurette-divider red">
		
		<p class="text-center normal large"><em>
			web design, development, deployment<br>
			search engine optimization + marketing<br>
			social media strategy, design + management<br>
			promotions design + implementation<br>
			brand development</em>
		</p>
		</div>
	</div>
	
	<div class="col-lg-6 pad-bot pad-top">
		<div class="col-lg-4"><img class="offerings img-responsive img-center" src="<?php echo get_template_directory_uri(); ?>/assets/img/events.png"></div>
		<div class="col-lg-8">
		<hr class="featurette-divider red">
		<h3 class="text-center uppercase">Events Orchestration</h3>
		<hr class="featurette-divider red">
		
		<p class="text-center normal large"><em>
			talent selection + arrangement<br>
			venue selection + contracting<br>
			strategy development + implementation<br>
			success celebration<br>
			tradeshows Design and Coordination</em>
		</p>
		</div>
	</div>

	<div class="col-lg-6 pad-bot pad-top">
		<div class="col-lg-4"><img class="offerings img-responsive img-center pad-bot" src="<?php echo get_template_directory_uri(); ?>/assets/img/training.png"></div>
		<div class="col-lg-8">
		<hr class="featurette-divider red">
		<h3 class="text-center uppercase">Training + Education</h3>
		<hr class="featurette-divider red">
		
		<p class="text-center normal large"><em>
			customer response<br>
			call center development<br>
			motivation + leadership<br>
			quality assurance<br>
			team building</em>
		</p>
		</div>
	</div>

	<div class="row pad-top pad-bot"></div>
	<hr>
	<div class="col-lg-6">
		<p class="normal"></p>
		<p>By drawing on our wealth of experience in creating, growing and exiting businesses of our own, we can share the expertise we’ve gained in nearly every aspect of organizational development with you.</p> 
		<p>We know how to help you engage your customers, your team members, your investors, your business partners and the public. Communication is, of course, key: an accurate vision statement, a meaningful, truthful mission statement and a concise, and a thoughtful list of core values associated with your organization, are invaluable tools that will underlie your future success. Ongoing, professional communications that speak clearly, concisely and in a single “corporate voice” to all of these constituents is important to your image and the development of your lasting reputation. Our corporate communications skills are proven.</p>
	</div>
	
	<div class="col-lg-6">
		<blockquote>
			<p class="thin"><i class="icon-quote-left"> </i>Having grown up in the family moving and storage business, I learned very early on that to be successful a company requires “whatever it takes” attitude on the part of its leadership.</p>
			<p class="thin">From sweeping the warehouse floors to expanding Muhlenhaupt Movers in industry-leading and exciting ways to divesting of those businesses and now focusing on others’ success has provided me keen insight and ever-changing ways to look at the challenges that face growing businesses.</p>
			<p class="thin">Collaborating closely with everyone involved while listening intently— and genuinely appreciating every individual’s contribution — is key to every Muhlenhaupt engagement. <i class="icon-quote-right"></i></p>
			<footer class="black">Ronald P. "Rip" Muhlenhaupt</footer>
		</blockquote>		
	</div>

</div> <!-- End Container -->
<div class="row"></div>
<?php get_template_part('templates/org'); ?>






