<div class="pad-top-2"></div>

<a href="https://twitter.com/MuhlenhauptCo"><i class="fa fa-twitter pad-bot" style="color:#55acee; padding-right:2px; font-size:22px;">  @MuhlenhauptCo</i></a>
<?php echo do_shortcode('[wolf_tweet username="MuhlenhauptCo" type="list" count="3"]'); ?>

<a href="https://www.linkedin.com/company/2516159?trk=tyah" target="_blank" class="btn btn-default">
	<img class="img-responsive img-center pad-top" src="<?php echo get_template_directory_uri(); ?>/assets/img/follow-linkedin.png" style="max-width:230px;">
</a>

<div class="pad-top pad-bot">
	<div class="black-bg row row-centered">
		<div class="col-lg-10 col-centered text-left pad-top pad-bot">
			<h3><span class="white">LET US HELP YOUR</span> <span class="red">ORGANIZATION MOVE FORWARD.</span></h3>
			<?php echo do_shortcode('[contact-form-7 id="57" title="Contact form 1"]'); ?>
		</div>
	</div>
</div>

