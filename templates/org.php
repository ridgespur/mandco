<div class="black-bg">
	<p>&nbsp;</p>
	<div class="container pad-bot">
		<hr class="featurette-divider white">
		<p class="text-center uppercase pad-bot-2">
			
			<span class="white bold huge">ORGANIZATIONS THAT WE’VE HELPED </span>
			<span class="red bold huge">MOVE FORWARD.</span>



		</p>

		<ul class="gray-light threeColumns thin large">
			<li>Access Information Management</li>
			<li>Retrievex, Inc.</li>
			<ul>
				<li>The FileLine System</li>
			</ul>
			<li>Hayden Real Estate Investments</li>
			<ul>
				<li>MIM-Hayden Funds</li>
				<li>Hayden Management</li>
			</ul>
			<li>Paoli Executive Transport</li>
			<li>City Avenue District - Philadelphia</li>
			<li>Parish Enthronement</li>
			<li>RenovatingHope</li>
			<li>Chesterbrook Dental Associates</li>
			<li>Pioneer Capital</li>
			<li>Rad&Kell</li>
			<li>Gallagher Advisory</li>
			<li>Sucker-Punch Productions</li>
			<li>Merion Construction</li>
			<li>Sacred Heart Enthronement</li>
			<li>Peggie O'Neill / Prayer Power</li>
			<li>The Carnegie Abbey Club</li>
			<ul>
				<li>World Leadership Guest Series</li>
			</ul>
			<li>O'Neill Properties Group</li>
			<ul>
				<li>The Carnegie Tower</li>
				<li>Uptown Worthington</li>
				<li>The Laureate Club</li>
				<li>The Newport Club</li>
			</ul>
			<li>Holy Love Ministries</li>
			<li>Frederick Jay Hill Orthodontics</li>

		</ul>
	</div>
</div>