<div class="black-bg pad-top pad-bot">
  <div class="container ">

    <div class="col-lg-6">
      <p class="" style="padding-top:4px;color:#929292;">&copy; <?php echo date("Y"); ?> Muhlenhaupt + Company, LLC. All rights reserved.</p>
    </div>

    <div class="col-lg-4"><?php get_template_part('templates/mailchimp'); ?></div>

    <div class="col-lg-2 text-right">
      <span class="fa-stack fa-lg">
        <a target="_blank" href="https://twitter.com/MuhlenhauptCo">
          <i class="fa fa-circle fa-stack-2x red"></i>
          <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
        </a>
      </span>

      <span class="fa-stack fa-lg">
        <a target="_blank" href="http://instagram.com/MovingCompaniesForward">
          <i class="fa fa-circle fa-stack-2x red"></i>
          <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
        </a>
      </span>

      <span class="fa-stack fa-lg">
        <a target="_blank" href="https://www.linkedin.com/company/2516159?trk=tyah">
          <i class="fa fa-circle fa-stack-2x red"></i>
          <i class="fa fa-linkedin fa-stack-1x fa-inverse"></i>
        </a>
      </span>

      <span class="fa-stack fa-lg">
        <a target="_blank" href="https://plus.google.com/+Movingcompaniesforward">
          <i class="fa fa-circle fa-stack-2x red"></i>
          <i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
        </a>
      </span>
    </div>

    <div class="white" style="padding-left:15px;">
      <p><i class="fa fa-phone" style="padding-right:4px;"> </i> 610.955.8172 | <a href="mailto:insight@muhlenhaupt.com?Subject=FooterContactForm" target="_top"><i class="fa fa-envelope-o" style="padding:0px 4px;"> </i> insight@muhlenhaupt.com</a></p>
      <p>1100 East Hector Street, Suite 200  |  Conshohocken, PA  19428 | <a href="https://www.google.com/maps/place/1100+E+Hector+St+%23200,+Conshohocken,+PA+19428/@40.075954,-75.287698,16z/data=!4m2!3m1!1s0x89c6be8cd5e91e53:0xb4f9f905282f2410?hl=en-GB">Get Directions</a></p>
    </div>

  </div>
</div>