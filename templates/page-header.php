<div>

	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	  <!-- Wrapper for slides -->
	  <div class="carousel-inner" role="listbox">
	    <div class="item active">
	      <?php if ( is_single() ) { ?>
			<img class="img-responsive full-width" src="<?php echo get_template_directory_uri(); ?>/assets/img/half_full.jpg" alt="...">
	      <?php } else { ?>
			<img class="img-responsive full-width" src="<?php echo get_template_directory_uri(); ?>/assets/img/banner.png" alt="...">
	      <?php } ?>
	      <div class="carousel-caption">
	        <h1 class="title-text black">
	        	<hr class="featurette-divider black">
	        	<span><?php echo roots_title(); ?></span>
	        </h1>
	      </div>
	    </div>
	  </div>
	</div>

</div>