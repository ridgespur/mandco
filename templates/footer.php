<footer class="navbar navbar-default navbar-static-top container content-info pad-bot" role="contentinfo">
    
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      
      <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/footer-logo.png" alt="..."></a>  
      <!-- <img class="img-responsive " src="<?php echo get_template_directory_uri(); ?>/assets/img/pen.png" alt="..."> -->

    </div>

    <nav class="collapse navbar-collapse" role="navigation">
      <?php
        if (has_nav_menu('footer_navigation')) :
          wp_nav_menu(array('theme_location' => 'footer_navigation', 'walker' => new Roots_Nav_Walker(), 'menu_class' => 'nav white navbar-nav'));
        endif;
      ?>   
      
    </nav>
    
    <div class="col-lg-4">
    	<?php dynamic_sidebar('sidebar-footer'); ?>
    </div>

    

</footer>
