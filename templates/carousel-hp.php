
<div id="carousel-id" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
        <li data-target="#carousel-id" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-id" data-slide-to="1" class=""></li>
        <li data-target="#carousel-id" data-slide-to="2" class=""></li>
    </ol>
    <div class="carousel-inner">
        <div class="item active">
            <img class="img-responsive img-center" src="<?php echo get_template_directory_uri(); ?>/assets/img/homepage_one.jpg">
            <!-- <div class="container">
                <div class="carousel-caption">
                    
                    <p>&nbsp;</p>
                    <hr class="featurette-divider red">
					<h1 class="text-center red">ACCESS. INFORMATION PROTECTED.</h1>
					<hr class="featurette-divider red">
                    <p class="black">In October of 2012, Access acquired Retrievex and needed a strong and powerful new brand. Incorporating the best of the two brands, we established the brand for the largest privately-held company in their industry. <b>Nationwide.</b></p>
                </div>
            </div> -->
        </div>
        <div class="item">
            <img class="img-responsive img-center" src="<?php echo get_template_directory_uri(); ?>/assets/img/homepage_two.jpg">
            <!-- <div class="container">
                <div class="carousel-caption">
                    
                    <h1>Another example headline.</h1>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                    <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
                </div>
            </div> -->
        </div>
        <div class="item ">
            <img class="img-responsive img-center" src="<?php echo get_template_directory_uri(); ?>/assets/img/homepage_three.jpg">
            <!-- <div class="container">
                <div class="carousel-caption">
                    
                    <h1>One more for good measure.</h1>
                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                    <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
                </div>
            </div> -->
        </div>
    </div>
    <a class="left carousel-control" href="#carousel-id" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
    <a class="right carousel-control" href="#carousel-id" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
</div>