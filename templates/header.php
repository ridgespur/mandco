<header class="banner navbar navbar-default navbar-static-top black-bg" role="banner">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>"><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/img/logo-white.png" alt="..."></a>
    </div>

    <div class="pull-right" style="padding-top:32px;">
      <!-- <a class="white normal white-links" href="" data-toggle="modal" data-target="#contact-form" style="padding-right:10px;" >Contact Us</a> -->
      <a class="white normal white-links" href="" data-toggle="modal" data-target="#search-form" style="padding-right:10px;"><i class="fa fa-search"> </i> Search </a>
      <i class="fa fa-phone white"> </i>
      <a href="tel:16109173700" class="white normal white-links"> 610.917.3700</a>
      
    </div>

    <nav class="collapse navbar-collapse" role="navigation">
      <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(array('theme_location' => 'primary_navigation', 'walker' => new Roots_Nav_Walker(), 'menu_class' => 'nav white navbar-nav'));
        endif;
      ?>
    </nav>
  </div>
</header>



<!-- Modal Search -->
<div class="modal fade" id="search-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Search Muhlenhaupt + Company</h4>
      </div>
      <div class="modal-body">
      <?php get_template_part('templates/searchform'); ?>
      </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
    </div>
  </div>
</div>

<!-- Modal Contact -->
<div class="modal fade" id="contact-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-body">
        
        <div class="pad-top pad-bot">
          <div class="black-bg row row-centered">
            <div class="col-lg-10 col-centered text-left pad-top pad-bot">
              <h3><span class="white">LET US HELP YOUR</span> <span class="red">ORGANIZATION MOVE FORWARD.</span></h3>
              <?php echo do_shortcode('[contact-form-7 id="57" title="Contact form 1"]'); ?>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>