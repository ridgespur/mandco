<?php

$red_banner_white = get_field('red_banner_white');
$red_banner_black = get_field('red_banner_black');
$read_more_text = get_field('read_more_text');
$read_more_link = get_field('read_more_link');
$read_more_link_text = get_field('read_more_link_text');
?>

<?php if ($red_banner_white || $red_banner_black != null ) {
	?>
	<div class="red-bg">
		<p class="bold huge text-center uppercase pad-top pad-bot" style="margin-bottom: 0px;">		
			<span class="white bold huge"><?php echo $red_banner_white; ?></span>
			<span class="black"><?php echo $red_banner_black; ?></span>
		</p>
	</div>
	<?php
} elseif ( is_page_template( 'template-half.php' ) || in_category( 'half-full' ) ) { 
	?>
	<div class="<?php if ( is_single() ) { echo ''; } else echo 'texture'; ?>">
		<img class="img-responsive img-center pad-top-2 pad-bot" src="<?php echo get_template_directory_uri(); ?>/assets/img/Half-Full-Logo.png" style="padding-left:10px;padding-right: 10px;">		
	</div>
	<?php
} else {
	?>
	<div class="red-bg">
		<p class="bold huge text-center uppercase pad-top pad-bot" style="margin-bottom: 0px;">	
			<span class="white bold huge"><?php echo roots_title(); ?></span>
		</p>
	</div>
	<?php
} ?>

<?php if ($read_more_text != null && !is_search() ) {
	?>
	<div class="texture pad-bot">
		<div class="container normal lato medium text-center">
			&nbsp;
			<hr class="featurette-divider black">
			<p class="banner"><?php echo $read_more_text; ?></p>
			
			<?php if ($read_more_link_text != null) { ?> 
				<a class="red" href="<?php echo $read_more_link; ?>"><?php echo $read_more_link_text; ?></a>
			<?php } ?>
			
		</div>
	</div>
	<?php
}
?>

