<?php //get_template_part('templates/page', 'header'); ?>
<?php //get_template_part('templates/banner'); ?>

<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(  ); ?>>
    <div class="container pad-top pad-bot"> <!-- my container -->
      <header>
        <!-- <h1 class="entry-title"><?php the_title(); ?></h1> -->
        <?php //get_template_part('templates/entry-meta'); ?>
      </header>
    
      <div class="entry-content">
        <?php 
          //if ( has_post_thumbnail() ) {
          //echo the_post_thumbnail( array(225,225), array( 'class' => 'img-responsive' ) );
        //} ?>
        
        <?php the_content(); ?>
      </div>
    </div>
    
    <footer>
      <?php wp_link_pages(array('before' => '<nav class="page-nav"><p>' . __('Pages:', 'roots'), 'after' => '</p></nav>')); ?>
    </footer>
    <?php //comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
