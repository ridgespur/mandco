<div class="pad-top-2"></div>

<?php dynamic_sidebar('sidebar-primary'); ?>

<div class="pad-top pad-bot">
	<div class="black-bg row row-centered">
		<div class="col-lg-10 col-centered text-left pad-top pad-bot">
			<h3><span class="white">LET US HELP YOUR</span> <span class="red">ORGANIZATION MOVE FORWARD.</span></h3>
			<?php echo do_shortcode('[contact-form-7 id="57" title="Contact form 1"]'); ?>
		</div>
	</div>
</div>